//
// Created by enemy1205 on 2021/10/6.
//
#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using cv::Mat;
using cv::Point;

class Hough {
public:
    Mat src;
    Mat bin;
    Mat counter;
    int threshold;//投票通过最小阈值
    int halfDistWindowSize;
    std::vector<Point> potentialPoints;

    virtual void vote(){};
};

class LineDetect : public Hough {
public:
    explicit LineDetect(Mat &);

    LineDetect(Mat &, int &);

    void calCounter(const int &, const int &);

    float maxDist;//最大距离
    int thetaDim;//角度划分刻度数
    int distDim;//距离划分刻度数
    void vote() override;

    void filter();

    void draw();

    void run(const int &, const int &);
};

class CircleDetect : public Hough {
    void vote() override;
};

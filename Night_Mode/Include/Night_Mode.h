//
// Created by enemy1205 on 2021/8/31.
//

#ifndef LEARNING_NIGHT_MODE_H
#define LEARNING_NIGHT_MODE_H
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/photo.hpp>
using namespace cv;
//导向滤波器
class GuidedFilterImpl
{
public:
    virtual ~GuidedFilterImpl() = default;

    Mat filter(const Mat &p, int depth);
protected:
    int Idepth;

private:
    virtual Mat filterSingleChannel(const Mat &p) const = 0;
};
class GuidedFilterMono : public GuidedFilterImpl
{
public:
    GuidedFilterMono(const Mat &I, int r, double eps);

private:
    Mat filterSingleChannel(const Mat &p) const override;

private:
    int r;
    double eps;
    Mat I, mean_I, var_I;
};
class GuidedFilterColor : public GuidedFilterImpl
{
public:
    GuidedFilterColor(const Mat &I, int r, double eps);

private:
    Mat filterSingleChannel(const Mat &p) const override;

private:
    std::vector<Mat> Ichannels;
    int r;
    double eps;
    Mat mean_I_r, mean_I_g, mean_I_b;
    Mat invrr, invrg, invrb, invgg, invgb, invbb;
};

class GuidedFilter
{
public:
    GuidedFilter(const Mat &I, int r, double eps);
    ~GuidedFilter();

    Mat filter(const Mat &p, int depth = -1) const;

private:
    GuidedFilterImpl *impl_;
};
Mat guidedFilter(const Mat &I, const Mat &p, int r, double eps, int depth = -1);
void run(std::string);
#endif //LEARNING_NIGHT_MODE_H

/**
 * @file SingleTracker.h
 * @author enemy1205 (enemy1205@qq.com)
 * @date 2021-09-02
 */

#ifndef LEARNING_SINGLETRACKER_H
#define LEARNING_SINGLETRACKER_H
#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/tracking/tracking_legacy.hpp>
#include <opencv2/core/ocl.hpp>

using namespace cv;
using namespace std;
//单目标检测器类
class SingleTracker{
    VideoCapture VC;
    string trackerType;
    // Create a tracker 创建跟踪器
    Ptr<Tracker> tracker;
    Ptr<legacy::Tracker>LE_tracker;
    //跟踪算法类型
    string trackerTypes[7] = { "BOOSTING", "MIL", "KCF", "TLD","MEDIANFLOW","MOSSE", "CSRT" };
    bool legacy= false;
public:
    void run();
    SingleTracker(const string & video_path);
    SingleTracker(const string & video_path,int i);
};
#endif //LEARNING_SINGLETRACKER_H

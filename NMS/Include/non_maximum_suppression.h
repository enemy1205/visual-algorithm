#pragma once

#include <opencv2/core/core.hpp>
#include<vector>

class NMS{
void sort(std::vector<cv::RotatedRect> &);
cv::RotatedRect& Non_max_sup(std::vector<cv::RotatedRect> &);
void calculate(std::vector<cv::RotatedRect> &);
float iou;//
};
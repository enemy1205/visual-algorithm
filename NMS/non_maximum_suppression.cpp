#include "non_maximum_suppression.h"
#include <algorithm>
using namespace cv;
using namespace std;

void NMS::sort(std::vector<cv::RotatedRect> &rects)
{
    //由于识别到的矩形框均是无偏角的标准矩形框，故角点有顺序，在此不做处理
    //按照中心点进行排序
    std::sort(rects.begin(), rects.end(), [&](RotatedRect &rect1, RotatedRect &rect2)
              { return rect1.center.x < rect2.center.x; });
}

void NMS::calculate(std::vector<cv::RotatedRect> &rects)
{
    for (size_t i = 0; i < rects.size(); i++)
    {
        Point2f pt1[4], pt2[4];
        rects[i].points(pt1);
        rects[i + 1].points(pt2);
        //右边矩形框包裹左边
        if (pt1[1].x > pt2[1].x&&pt1[1].y>pt2[1].y)
        {
            
        }
    }
}

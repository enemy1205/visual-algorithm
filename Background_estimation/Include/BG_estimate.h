/**
 * @file BG_estimate.h
 * @author enemy1205 (enemy1205@qq.com)
 * @date 2021-08-30
 */
#pragma once

#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include <random>
#include <opencv2/bgsegm.hpp>
#include <iostream>
using namespace std;
using namespace cv;

class Frame_difference {
    Mat result;

    int computeMedian(vector<int> &elements);

public:
    Mat Get_compute_median() { return result; }

    explicit Frame_difference(vector<Mat> vec);
};

class BG_estimate {
public:
    BG_estimate() { std::cout << "倘若要输入指定视频路径请重开程序在构造时加入路径参数" << endl; };//默认路径
    explicit BG_estimate(string);//指定路径
    void run();

    void api_run();

private:
    string video_file = "../resources/video.mp4";
};

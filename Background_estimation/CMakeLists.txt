aux_source_directory(. BG_estimate_DIR)
set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/lib)
add_library(Background_estimation ${BG_estimate_DIR})
target_include_directories(Background_estimation PUBLIC Include)
/**
 * @file Controller.cpp
 * @author enemy1205 (enemy1205@qq.com)
 * @brief 主控制类
 * @date 2021-09-19
 */
#include "Controller.h"

void VisualAlgorithm::execute(int &choice)
{
    startTime = clock(); //开始计时
    switch (choice)
    {
    case 0:
    { //MeanShift
        string path;
        cout << "请输入测试图像路径" << endl;
        cin >> path;
        MeanShift MS(path);
        MS.meanshiftSegmentation();
        imshow("res", MS.getResult());
        waitKey(0);
        destroyAllWindows();
        break;
    }
    case 1:
    { //WaterShed
        //    string path="/home/enemy1205/文档/Resources/cards.jpg";
        //    WaterShed WS(path);
        string path;
        WaterShed WS;
        cin >> path;
        WS.Init(imread(path));
        WS.waterShedWithMarkers();
        break;
    }
    case 2:
    { //Saliency
        Saliency_demo SA;
        SA.run();
        break;
    }
    case 3:
    { //FourierTransform
        string path;
        cout << "请输入测试图像路径" << endl;
        cin >> path;
        Mat img = imread(path);
        FourierTransform FT(img);
        break;
    }
    case 4:
    { //BG_estimate
        string path;
        cout << "请输入测试视频路径" << endl;
        cin >> path;
        BG_estimate bgEstimate(path);
        bgEstimate.api_run();
        break;
    }
    case 5:
    { //Night_Mode
        string path;
        cout << "请输入测试图像路径" << endl;
        cin >> path;
        run(path);
        break;
    }
    case 6:
    { //SingleTracker
        string path;
        cout << "请输入测试视频路径" << endl;
        cin >> path;
        SingleTracker ST(path);
        ST.run();
        break;
    }
    case 7:
    { //KNN
        KNN knn;
        vector<double> P = {5, 4};
        vector<vector<double>> data = {{2, 4},
                                       {5, 6},
                                       {9, 8},
                                       {4, 2},
                                       {8, 3},
                                       {7, 5},
                                       {6, 11},
                                       {15, 16}};
        //            knn.test_knn();
        knn.run(data, P, 4);
        break;
    }
    case 8:
    { //Canny边缘检测
        string path = "/home/enemy1205/图片/壁纸/54872584.png";
        //            cout << "请输入测试视频路径" << endl;
        //            cin >> path;
        EdgeDetection ED(path, 5);
        ED.Edge_Canny();
        break;
    }

    case 9:
    { //huogh直线检测
        Mat img = imread("/home/enemy1205/文档/Resources/shapes.png");
        LineDetect lineDetect(img);
        lineDetect.run(90, 2);
        break;
    }
    // case 10:
    // {
    //     NMS nms;
    //     nms.Non_max_sup();
    // }
    default:
        break;
    }

    endTime = clock(); //结束计时
    double interval_time = double(endTime - startTime) / CLOCKS_PER_SEC;
    std::cout << interval_time << "s" << std::endl;
}

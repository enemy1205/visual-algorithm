/**
 * @file Controller.h
 * @author enemy1205 (enemy1205@qq.com)
 * @brief 主控制类头文件
 * @date 2021-09-19
 */
#include "BG_estimate.h"
#include "Night_Mode.h"
#include "Saliency.h"
#include "SingleTracker.h"
#include "Watershed.h"
#include "MeanShift.h"
#include "Fourier_transform.h"
#include "KNN.h"
#include "EdgeDetection.h"
#include "HoughDetect.h"
#include "non_maximum_suppression.h"
#include <ctime>
#include <cstdint>
#include <iostream>

class VisualAlgorithm
{
public:
    VisualAlgorithm() = default;
    void execute(int &);

protected:
    clock_t startTime, endTime; //计时器
};

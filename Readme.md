#  VisualAlgorithm 

#### 测试说明

+ 需要提前安装的依赖库

  > opencv 4.x

> ###### 在工程目录下打开终端
>
> ###### mkdir build
>
> ###### cd build/
>
> ###### cmake -对应模块/文件名=ON ..
>
> `示例 cmake -Saliency=ON ..`
>
> ###### make -j8
>
> ###### ./Algorithm
>
> ###### 即可开始运行

**文件架构**

```txt
.
├── Background_estimation
│   ├── BG_estimate.cpp
│   ├── CMakeLists.txt
│   └── Include
│       └── BG_estimate.h
├── CMakeLists.txt
├── Controller
│   ├── CMakeLists.txt
│   ├── Controller.cpp
│   └── Include
│       └── Controller.h
├── Fourier_transform
│   ├── CMakeLists.txt
│   ├── Fourier_transform.cpp
│   └── Include
│       └── Fourier_transform.h
├── main.cpp
├── MeanShift
│   ├── CMakeLists.txt
│   ├── Include
│   │   └── MeanShift.h
│   └── MeanShift.cpp
├── Night_Mode
│   ├── brighten.cpp
│   ├── CMakeLists.txt
│   ├── guided_filter.cpp
│   └── Include
│       └── Night_Mode.h
├── Readme.md
├── resources
│   ├── chaplin.mp4
│   ├── dark.png
│   └── video.mp4
├── KNN
│   ├── CMakeLists.txt
│   ├── Include
│   │   └── KNN.h
│   ├── KdTree.cpp
│   └── KNN.cpp
├── Saliency
│   ├── CMakeLists.txt
│   ├── Include
│   │   └── Saliency.h
│   └── Saliency.cpp
├── SingleTracker
│   ├── CMakeLists.txt
│   ├── Include
│   │   └── SingleTracker.h
│   └── SingleTracker.cpp
└── Watershed
    ├── CMakeLists.txt
    ├── Include
    │   └── Watershed.h
    └── Watershed.cpp


```


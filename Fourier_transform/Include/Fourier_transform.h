/**
 * @file Fourier_transform.h
 * @author enemy1205 (enemy1205@qq.com)
 * @brief 傅里叶变换头文件
 * @date 2021-09-18
 */
#pragma once
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using std::cout;
using std::endl;
class FourierTransform{
public:
    explicit FourierTransform(Mat&);
    void ImageDFT();
    void resort(Mat&);

private:
    Mat src;
    Mat fImg;//float64位图像
    Mat fftImage;//复数图像值
    Mat res;//逆变换图
};

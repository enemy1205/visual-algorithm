/**
 * @file Saliency.h
 * @author enemy1205 (enemy1205@qq.com)
 * @brief 显著性检验头文件
 * @date 2021-09-02
 */
#ifndef LEARNING_SALIENCY_H
#define LEARNING_SALIENCY_H
#include <opencv2/saliency.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
using namespace cv;
using namespace std;
using namespace saliency;
//显著性检测类
class Saliency_demo{
    int mode;
    Mat src;
    Mat saliencyMap;	// 检测结果，白色区域表示显著区域
    Mat binaryMap;
    string training_path;
    string video_path="../resources/video.mp4";
    // 实例化saliencyAlgorithm结构
    Ptr<saliency::Saliency> saliencyAlgorithm;
    void show();
    void SPECTRAL_RESIDUAL(const Mat &);
    void FineGrained(const Mat &);
    void ObjectnessBING(const Mat &);
    void BinWangApr2014();

public:
    void run();
    Saliency_demo();
};
#endif //LEARNING_SALIENCY_H

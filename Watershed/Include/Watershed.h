/**
 * @file Watershed.h
 * @author enemy1205 (enemy1205@qq.com)
 * @brief 分水岭算法头文件
 * @date 2021-09-08
 */
#ifndef LEARNING_WATERSHED_H
#define LEARNING_WATERSHED_H

#include <opencv2/opencv.hpp>
#include <random>
#include <queue>
using namespace std;
using namespace cv;
//-------------分割类中命名空间----------
namespace Segmentation {

    const int SHED = -1;//分水岭
    const int INQUEUE = -2;

    struct point2D {
        int r, c;//行，列,与后面R,C对应
    };

    struct RGB {
        uchar r, g, b;

        RGB(int R, int G, int B) {
            b = uchar(B);
            g = uchar(G);
            r = uchar(R);
        }
    };
}

//-----------------分水岭算法类----------------
class WaterShed{
    Mat src;              //图像源图
    int basins;           //盆地,即分割区域标志数
    Mat markersImg;       //标记层
    Mat drawImg;          //绘图层
    Mat resMask;          //特征蒙版矩阵
    Mat result;           //分割结果彩图,父类中
    int thickness;        //画笔宽度
    Scalar drawColor;     //画笔颜色
public:
    WaterShed(string &);

    explicit WaterShed() { this->help(); };

    void Init(const Mat &);

    void markers();

    Mat preProcess();

    void waterShedWithMarkers();

    void colorMask();

    void ColorMask();

    void findLocalMinimum();

    void waterShedAuto();

    void waterShedVideo(const string &);

    void help();
};

#endif //LEARNING_WATERSHED_H

/**
 * @file main.cpp
 * @author enemy1205 (enemy1205@qq.com)
 * @date 2021-08-30
 */
#include "Controller.h"

using namespace cv;
using namespace std;

int main() {
    int choice;
    cout << "请输入选择模块" << endl;
    cin >> choice;
    VisualAlgorithm visualAlgorithm;
    visualAlgorithm.execute(choice);
    return 0;
}
